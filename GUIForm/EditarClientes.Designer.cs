﻿namespace GUIForm
{
    partial class EditarClientes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditarClientes));
            this.DireccionTxt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.PaisTxt = new System.Windows.Forms.TextBox();
            this.PaisLbl = new System.Windows.Forms.Label();
            this.EstadoTxt = new System.Windows.Forms.TextBox();
            this.EstadoLbl = new System.Windows.Forms.Label();
            this.MunicipioTxt = new System.Windows.Forms.TextBox();
            this.MunicipioLbl = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.MetodoPagoTxt = new System.Windows.Forms.TextBox();
            this.NOIntTxt = new System.Windows.Forms.TextBox();
            this.ColoniaLbl = new System.Windows.Forms.Label();
            this.NoIntLbl = new System.Windows.Forms.Label();
            this.CPTxt = new System.Windows.Forms.TextBox();
            this.CPLbl = new System.Windows.Forms.Label();
            this.NombreClienteTxt = new System.Windows.Forms.TextBox();
            this.NombreEmpresaLbl = new System.Windows.Forms.Label();
            this.CerrarBtn = new System.Windows.Forms.Label();
            this.CelleTxt = new System.Windows.Forms.TextBox();
            this.TelefonoTxt = new System.Windows.Forms.TextBox();
            this.NombreContacto2Txt = new System.Windows.Forms.TextBox();
            this.EmailTxt = new System.Windows.Forms.TextBox();
            this.NombreContactoTxt = new System.Windows.Forms.TextBox();
            this.IDClienteTxt = new System.Windows.Forms.TextBox();
            this.CalleLbl = new System.Windows.Forms.Label();
            this.TelefonoLbl = new System.Windows.Forms.Label();
            this.EmailLbl = new System.Windows.Forms.Label();
            this.NombreContacto2Lbl = new System.Windows.Forms.Label();
            this.NombreContactoLbl = new System.Windows.Forms.Label();
            this.IDLbl = new System.Windows.Forms.Label();
            this.CancelarBtn = new System.Windows.Forms.Button();
            this.GuardarBtn = new System.Windows.Forms.Button();
            this.EditarClienteLbl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // DireccionTxt
            // 
            this.DireccionTxt.BackColor = System.Drawing.Color.LightGray;
            this.DireccionTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DireccionTxt.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DireccionTxt.Location = new System.Drawing.Point(708, 333);
            this.DireccionTxt.Multiline = true;
            this.DireccionTxt.Name = "DireccionTxt";
            this.DireccionTxt.Size = new System.Drawing.Size(258, 144);
            this.DireccionTxt.TabIndex = 94;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(546, 333);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(140, 20);
            this.label2.TabIndex = 93;
            this.label2.Text = "Dirección alterna:";
            // 
            // PaisTxt
            // 
            this.PaisTxt.BackColor = System.Drawing.Color.LightGray;
            this.PaisTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.PaisTxt.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PaisTxt.Location = new System.Drawing.Point(707, 296);
            this.PaisTxt.Name = "PaisTxt";
            this.PaisTxt.Size = new System.Drawing.Size(258, 19);
            this.PaisTxt.TabIndex = 92;
            // 
            // PaisLbl
            // 
            this.PaisLbl.AutoSize = true;
            this.PaisLbl.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PaisLbl.Location = new System.Drawing.Point(645, 295);
            this.PaisLbl.Name = "PaisLbl";
            this.PaisLbl.Size = new System.Drawing.Size(41, 20);
            this.PaisLbl.TabIndex = 91;
            this.PaisLbl.Text = "País:";
            // 
            // EstadoTxt
            // 
            this.EstadoTxt.BackColor = System.Drawing.Color.LightGray;
            this.EstadoTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.EstadoTxt.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EstadoTxt.Location = new System.Drawing.Point(707, 260);
            this.EstadoTxt.Name = "EstadoTxt";
            this.EstadoTxt.Size = new System.Drawing.Size(258, 19);
            this.EstadoTxt.TabIndex = 90;
            // 
            // EstadoLbl
            // 
            this.EstadoLbl.AutoSize = true;
            this.EstadoLbl.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EstadoLbl.Location = new System.Drawing.Point(624, 259);
            this.EstadoLbl.Name = "EstadoLbl";
            this.EstadoLbl.Size = new System.Drawing.Size(62, 20);
            this.EstadoLbl.TabIndex = 89;
            this.EstadoLbl.Text = "Estado:";
            // 
            // MunicipioTxt
            // 
            this.MunicipioTxt.BackColor = System.Drawing.Color.LightGray;
            this.MunicipioTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.MunicipioTxt.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MunicipioTxt.Location = new System.Drawing.Point(707, 223);
            this.MunicipioTxt.Name = "MunicipioTxt";
            this.MunicipioTxt.Size = new System.Drawing.Size(258, 19);
            this.MunicipioTxt.TabIndex = 88;
            // 
            // MunicipioLbl
            // 
            this.MunicipioLbl.AutoSize = true;
            this.MunicipioLbl.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MunicipioLbl.Location = new System.Drawing.Point(602, 223);
            this.MunicipioLbl.Name = "MunicipioLbl";
            this.MunicipioLbl.Size = new System.Drawing.Size(85, 20);
            this.MunicipioLbl.TabIndex = 87;
            this.MunicipioLbl.Text = "Municipio:";
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.LightGray;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(895, 112);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(70, 19);
            this.textBox1.TabIndex = 86;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(815, 111);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 20);
            this.label1.TabIndex = 85;
            this.label1.Text = "No. Int:";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.LightGray;
            this.panel1.Location = new System.Drawing.Point(535, 72);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(5, 405);
            this.panel1.TabIndex = 84;
            // 
            // MetodoPagoTxt
            // 
            this.MetodoPagoTxt.BackColor = System.Drawing.Color.LightGray;
            this.MetodoPagoTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.MetodoPagoTxt.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MetodoPagoTxt.Location = new System.Drawing.Point(707, 145);
            this.MetodoPagoTxt.Name = "MetodoPagoTxt";
            this.MetodoPagoTxt.Size = new System.Drawing.Size(258, 19);
            this.MetodoPagoTxt.TabIndex = 83;
            // 
            // NOIntTxt
            // 
            this.NOIntTxt.BackColor = System.Drawing.Color.LightGray;
            this.NOIntTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.NOIntTxt.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NOIntTxt.Location = new System.Drawing.Point(707, 108);
            this.NOIntTxt.Name = "NOIntTxt";
            this.NOIntTxt.Size = new System.Drawing.Size(70, 19);
            this.NOIntTxt.TabIndex = 82;
            // 
            // ColoniaLbl
            // 
            this.ColoniaLbl.AutoSize = true;
            this.ColoniaLbl.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ColoniaLbl.Location = new System.Drawing.Point(617, 145);
            this.ColoniaLbl.Name = "ColoniaLbl";
            this.ColoniaLbl.Size = new System.Drawing.Size(70, 20);
            this.ColoniaLbl.TabIndex = 81;
            this.ColoniaLbl.Text = "Colonia:";
            // 
            // NoIntLbl
            // 
            this.NoIntLbl.AutoSize = true;
            this.NoIntLbl.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NoIntLbl.Location = new System.Drawing.Point(625, 107);
            this.NoIntLbl.Name = "NoIntLbl";
            this.NoIntLbl.Size = new System.Drawing.Size(61, 20);
            this.NoIntLbl.TabIndex = 80;
            this.NoIntLbl.Text = "No. Int:";
            // 
            // CPTxt
            // 
            this.CPTxt.BackColor = System.Drawing.Color.LightGray;
            this.CPTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.CPTxt.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CPTxt.Location = new System.Drawing.Point(707, 185);
            this.CPTxt.Name = "CPTxt";
            this.CPTxt.Size = new System.Drawing.Size(258, 19);
            this.CPTxt.TabIndex = 79;
            // 
            // CPLbl
            // 
            this.CPLbl.AutoSize = true;
            this.CPLbl.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CPLbl.Location = new System.Drawing.Point(653, 185);
            this.CPLbl.Name = "CPLbl";
            this.CPLbl.Size = new System.Drawing.Size(34, 20);
            this.CPLbl.TabIndex = 78;
            this.CPLbl.Text = "CP:";
            // 
            // NombreClienteTxt
            // 
            this.NombreClienteTxt.BackColor = System.Drawing.Color.LightGray;
            this.NombreClienteTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.NombreClienteTxt.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NombreClienteTxt.Location = new System.Drawing.Point(218, 182);
            this.NombreClienteTxt.Name = "NombreClienteTxt";
            this.NombreClienteTxt.Size = new System.Drawing.Size(295, 19);
            this.NombreClienteTxt.TabIndex = 77;
            // 
            // NombreEmpresaLbl
            // 
            this.NombreEmpresaLbl.AutoSize = true;
            this.NombreEmpresaLbl.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NombreEmpresaLbl.Location = new System.Drawing.Point(42, 182);
            this.NombreEmpresaLbl.Name = "NombreEmpresaLbl";
            this.NombreEmpresaLbl.Size = new System.Drawing.Size(155, 20);
            this.NombreEmpresaLbl.TabIndex = 76;
            this.NombreEmpresaLbl.Text = "Nombre del Cliente:";
            // 
            // CerrarBtn
            // 
            this.CerrarBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CerrarBtn.AutoSize = true;
            this.CerrarBtn.BackColor = System.Drawing.Color.WhiteSmoke;
            this.CerrarBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CerrarBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CerrarBtn.Location = new System.Drawing.Point(947, 12);
            this.CerrarBtn.Name = "CerrarBtn";
            this.CerrarBtn.Size = new System.Drawing.Size(18, 18);
            this.CerrarBtn.TabIndex = 75;
            this.CerrarBtn.Text = "X";
            this.CerrarBtn.Click += new System.EventHandler(this.CerrarBtn_Click);
            // 
            // CelleTxt
            // 
            this.CelleTxt.BackColor = System.Drawing.Color.LightGray;
            this.CelleTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.CelleTxt.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CelleTxt.Location = new System.Drawing.Point(707, 73);
            this.CelleTxt.Name = "CelleTxt";
            this.CelleTxt.Size = new System.Drawing.Size(258, 19);
            this.CelleTxt.TabIndex = 74;
            // 
            // TelefonoTxt
            // 
            this.TelefonoTxt.BackColor = System.Drawing.Color.LightGray;
            this.TelefonoTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TelefonoTxt.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TelefonoTxt.Location = new System.Drawing.Point(218, 296);
            this.TelefonoTxt.Name = "TelefonoTxt";
            this.TelefonoTxt.Size = new System.Drawing.Size(295, 19);
            this.TelefonoTxt.TabIndex = 73;
            // 
            // NombreContacto2Txt
            // 
            this.NombreContacto2Txt.BackColor = System.Drawing.Color.LightGray;
            this.NombreContacto2Txt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.NombreContacto2Txt.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NombreContacto2Txt.Location = new System.Drawing.Point(218, 258);
            this.NombreContacto2Txt.Name = "NombreContacto2Txt";
            this.NombreContacto2Txt.Size = new System.Drawing.Size(295, 19);
            this.NombreContacto2Txt.TabIndex = 72;
            // 
            // EmailTxt
            // 
            this.EmailTxt.BackColor = System.Drawing.Color.LightGray;
            this.EmailTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.EmailTxt.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EmailTxt.Location = new System.Drawing.Point(217, 333);
            this.EmailTxt.Name = "EmailTxt";
            this.EmailTxt.Size = new System.Drawing.Size(295, 19);
            this.EmailTxt.TabIndex = 71;
            // 
            // NombreContactoTxt
            // 
            this.NombreContactoTxt.BackColor = System.Drawing.Color.LightGray;
            this.NombreContactoTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.NombreContactoTxt.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NombreContactoTxt.Location = new System.Drawing.Point(218, 219);
            this.NombreContactoTxt.Name = "NombreContactoTxt";
            this.NombreContactoTxt.Size = new System.Drawing.Size(295, 19);
            this.NombreContactoTxt.TabIndex = 70;
            // 
            // IDClienteTxt
            // 
            this.IDClienteTxt.BackColor = System.Drawing.Color.LightGray;
            this.IDClienteTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.IDClienteTxt.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDClienteTxt.Location = new System.Drawing.Point(218, 146);
            this.IDClienteTxt.Name = "IDClienteTxt";
            this.IDClienteTxt.ReadOnly = true;
            this.IDClienteTxt.Size = new System.Drawing.Size(295, 19);
            this.IDClienteTxt.TabIndex = 69;
            // 
            // CalleLbl
            // 
            this.CalleLbl.AutoSize = true;
            this.CalleLbl.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CalleLbl.Location = new System.Drawing.Point(635, 73);
            this.CalleLbl.Name = "CalleLbl";
            this.CalleLbl.Size = new System.Drawing.Size(51, 20);
            this.CalleLbl.TabIndex = 68;
            this.CalleLbl.Text = "Calle:";
            // 
            // TelefonoLbl
            // 
            this.TelefonoLbl.AutoSize = true;
            this.TelefonoLbl.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TelefonoLbl.Location = new System.Drawing.Point(122, 295);
            this.TelefonoLbl.Name = "TelefonoLbl";
            this.TelefonoLbl.Size = new System.Drawing.Size(75, 20);
            this.TelefonoLbl.TabIndex = 67;
            this.TelefonoLbl.Text = "Teléfono:";
            // 
            // EmailLbl
            // 
            this.EmailLbl.AutoSize = true;
            this.EmailLbl.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EmailLbl.Location = new System.Drawing.Point(147, 332);
            this.EmailLbl.Name = "EmailLbl";
            this.EmailLbl.Size = new System.Drawing.Size(50, 20);
            this.EmailLbl.TabIndex = 66;
            this.EmailLbl.Text = "Email:";
            // 
            // NombreContacto2Lbl
            // 
            this.NombreContacto2Lbl.AutoSize = true;
            this.NombreContacto2Lbl.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NombreContacto2Lbl.Location = new System.Drawing.Point(11, 258);
            this.NombreContacto2Lbl.Name = "NombreContacto2Lbl";
            this.NombreContacto2Lbl.Size = new System.Drawing.Size(186, 20);
            this.NombreContacto2Lbl.TabIndex = 65;
            this.NombreContacto2Lbl.Text = "Nombre del 2 Contacto:";
            // 
            // NombreContactoLbl
            // 
            this.NombreContactoLbl.AutoSize = true;
            this.NombreContactoLbl.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NombreContactoLbl.Location = new System.Drawing.Point(23, 219);
            this.NombreContactoLbl.Name = "NombreContactoLbl";
            this.NombreContactoLbl.Size = new System.Drawing.Size(174, 20);
            this.NombreContactoLbl.TabIndex = 64;
            this.NombreContactoLbl.Text = "Nombre del Contacto:";
            // 
            // IDLbl
            // 
            this.IDLbl.AutoSize = true;
            this.IDLbl.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDLbl.Location = new System.Drawing.Point(168, 145);
            this.IDLbl.Name = "IDLbl";
            this.IDLbl.Size = new System.Drawing.Size(29, 20);
            this.IDLbl.TabIndex = 63;
            this.IDLbl.Text = "ID:";
            // 
            // CancelarBtn
            // 
            this.CancelarBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CancelarBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.CancelarBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.CancelarBtn.FlatAppearance.BorderSize = 0;
            this.CancelarBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.CancelarBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.CancelarBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CancelarBtn.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CancelarBtn.ForeColor = System.Drawing.Color.White;
            this.CancelarBtn.Image = ((System.Drawing.Image)(resources.GetObject("CancelarBtn.Image")));
            this.CancelarBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.CancelarBtn.Location = new System.Drawing.Point(848, 486);
            this.CancelarBtn.Name = "CancelarBtn";
            this.CancelarBtn.Size = new System.Drawing.Size(120, 25);
            this.CancelarBtn.TabIndex = 62;
            this.CancelarBtn.Text = "Cancelar";
            this.CancelarBtn.UseVisualStyleBackColor = false;
            // 
            // GuardarBtn
            // 
            this.GuardarBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.GuardarBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.GuardarBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.GuardarBtn.FlatAppearance.BorderSize = 0;
            this.GuardarBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.GuardarBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.GuardarBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.GuardarBtn.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GuardarBtn.ForeColor = System.Drawing.Color.White;
            this.GuardarBtn.Image = ((System.Drawing.Image)(resources.GetObject("GuardarBtn.Image")));
            this.GuardarBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.GuardarBtn.Location = new System.Drawing.Point(707, 486);
            this.GuardarBtn.Name = "GuardarBtn";
            this.GuardarBtn.Size = new System.Drawing.Size(120, 25);
            this.GuardarBtn.TabIndex = 61;
            this.GuardarBtn.Text = "Guardar";
            this.GuardarBtn.UseVisualStyleBackColor = false;
            // 
            // EditarClienteLbl
            // 
            this.EditarClienteLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.EditarClienteLbl.AutoSize = true;
            this.EditarClienteLbl.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EditarClienteLbl.Location = new System.Drawing.Point(23, 12);
            this.EditarClienteLbl.Name = "EditarClienteLbl";
            this.EditarClienteLbl.Size = new System.Drawing.Size(146, 23);
            this.EditarClienteLbl.TabIndex = 60;
            this.EditarClienteLbl.Text = "EDITAR CLIENTE";
            // 
            // EditarClientes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(978, 523);
            this.Controls.Add(this.DireccionTxt);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.PaisTxt);
            this.Controls.Add(this.PaisLbl);
            this.Controls.Add(this.EstadoTxt);
            this.Controls.Add(this.EstadoLbl);
            this.Controls.Add(this.MunicipioTxt);
            this.Controls.Add(this.MunicipioLbl);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.MetodoPagoTxt);
            this.Controls.Add(this.NOIntTxt);
            this.Controls.Add(this.ColoniaLbl);
            this.Controls.Add(this.NoIntLbl);
            this.Controls.Add(this.CPTxt);
            this.Controls.Add(this.CPLbl);
            this.Controls.Add(this.NombreClienteTxt);
            this.Controls.Add(this.NombreEmpresaLbl);
            this.Controls.Add(this.CerrarBtn);
            this.Controls.Add(this.CelleTxt);
            this.Controls.Add(this.TelefonoTxt);
            this.Controls.Add(this.NombreContacto2Txt);
            this.Controls.Add(this.EmailTxt);
            this.Controls.Add(this.NombreContactoTxt);
            this.Controls.Add(this.IDClienteTxt);
            this.Controls.Add(this.CalleLbl);
            this.Controls.Add(this.TelefonoLbl);
            this.Controls.Add(this.EmailLbl);
            this.Controls.Add(this.NombreContacto2Lbl);
            this.Controls.Add(this.NombreContactoLbl);
            this.Controls.Add(this.IDLbl);
            this.Controls.Add(this.CancelarBtn);
            this.Controls.Add(this.GuardarBtn);
            this.Controls.Add(this.EditarClienteLbl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "EditarClientes";
            this.Opacity = 0.9D;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "EditarClientes";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox DireccionTxt;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox PaisTxt;
        private System.Windows.Forms.Label PaisLbl;
        public System.Windows.Forms.TextBox EstadoTxt;
        private System.Windows.Forms.Label EstadoLbl;
        public System.Windows.Forms.TextBox MunicipioTxt;
        private System.Windows.Forms.Label MunicipioLbl;
        public System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.TextBox MetodoPagoTxt;
        public System.Windows.Forms.TextBox NOIntTxt;
        private System.Windows.Forms.Label ColoniaLbl;
        private System.Windows.Forms.Label NoIntLbl;
        public System.Windows.Forms.TextBox CPTxt;
        private System.Windows.Forms.Label CPLbl;
        public System.Windows.Forms.TextBox NombreClienteTxt;
        private System.Windows.Forms.Label NombreEmpresaLbl;
        private System.Windows.Forms.Label CerrarBtn;
        public System.Windows.Forms.TextBox CelleTxt;
        public System.Windows.Forms.TextBox TelefonoTxt;
        public System.Windows.Forms.TextBox NombreContacto2Txt;
        public System.Windows.Forms.TextBox EmailTxt;
        public System.Windows.Forms.TextBox NombreContactoTxt;
        public System.Windows.Forms.TextBox IDClienteTxt;
        private System.Windows.Forms.Label CalleLbl;
        private System.Windows.Forms.Label TelefonoLbl;
        private System.Windows.Forms.Label EmailLbl;
        private System.Windows.Forms.Label NombreContacto2Lbl;
        private System.Windows.Forms.Label NombreContactoLbl;
        private System.Windows.Forms.Label IDLbl;
        private System.Windows.Forms.Button CancelarBtn;
        private System.Windows.Forms.Button GuardarBtn;
        private System.Windows.Forms.Label EditarClienteLbl;
    }
}