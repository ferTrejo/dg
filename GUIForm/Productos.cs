﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUIForm
{
    public partial class Productos : Form
    {
        public Productos()
        {
            InitializeComponent();
        }

        private void CerrarBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void NuevoBtn_Click(object sender, EventArgs e)
        {
            NuevoProducto frm = new NuevoProducto();
            frm.ShowDialog();
        }

        private void EditarBtn_Click(object sender, EventArgs e)
        {
            EditarProducto frm = new EditarProducto();
            frm.ShowDialog();
        }
    }
}
