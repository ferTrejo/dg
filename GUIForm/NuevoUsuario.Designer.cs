﻿namespace GUIForm
{
    partial class NuevoUsuario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NuevoUsuario));
            this.CerrarBtn = new System.Windows.Forms.Label();
            this.PassTxt = new System.Windows.Forms.TextBox();
            this.UsuarioTxt = new System.Windows.Forms.TextBox();
            this.ApellidoTxt = new System.Windows.Forms.TextBox();
            this.EmailTxt = new System.Windows.Forms.TextBox();
            this.NombreTxt = new System.Windows.Forms.TextBox();
            this.IDProductoTxt = new System.Windows.Forms.TextBox();
            this.PassLbl = new System.Windows.Forms.Label();
            this.UsuarioLbl = new System.Windows.Forms.Label();
            this.EmailLbl = new System.Windows.Forms.Label();
            this.ApellidoLbl = new System.Windows.Forms.Label();
            this.NombreLbl = new System.Windows.Forms.Label();
            this.IDLbl = new System.Windows.Forms.Label();
            this.CancelarBtn = new System.Windows.Forms.Button();
            this.GuardarBtn = new System.Windows.Forms.Button();
            this.NuevoUsuarioLbl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // CerrarBtn
            // 
            this.CerrarBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CerrarBtn.AutoSize = true;
            this.CerrarBtn.BackColor = System.Drawing.Color.WhiteSmoke;
            this.CerrarBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CerrarBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CerrarBtn.Location = new System.Drawing.Point(453, 3);
            this.CerrarBtn.Name = "CerrarBtn";
            this.CerrarBtn.Size = new System.Drawing.Size(18, 18);
            this.CerrarBtn.TabIndex = 38;
            this.CerrarBtn.Text = "X";
            this.CerrarBtn.Click += new System.EventHandler(this.CerrarBtn_Click);
            // 
            // PassTxt
            // 
            this.PassTxt.BackColor = System.Drawing.Color.LightGray;
            this.PassTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.PassTxt.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PassTxt.Location = new System.Drawing.Point(176, 237);
            this.PassTxt.Name = "PassTxt";
            this.PassTxt.Size = new System.Drawing.Size(295, 19);
            this.PassTxt.TabIndex = 37;
            // 
            // UsuarioTxt
            // 
            this.UsuarioTxt.BackColor = System.Drawing.Color.LightGray;
            this.UsuarioTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.UsuarioTxt.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UsuarioTxt.Location = new System.Drawing.Point(176, 203);
            this.UsuarioTxt.Name = "UsuarioTxt";
            this.UsuarioTxt.Size = new System.Drawing.Size(295, 19);
            this.UsuarioTxt.TabIndex = 36;
            // 
            // ApellidoTxt
            // 
            this.ApellidoTxt.BackColor = System.Drawing.Color.LightGray;
            this.ApellidoTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ApellidoTxt.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ApellidoTxt.Location = new System.Drawing.Point(176, 130);
            this.ApellidoTxt.Name = "ApellidoTxt";
            this.ApellidoTxt.Size = new System.Drawing.Size(295, 19);
            this.ApellidoTxt.TabIndex = 35;
            // 
            // EmailTxt
            // 
            this.EmailTxt.BackColor = System.Drawing.Color.LightGray;
            this.EmailTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.EmailTxt.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EmailTxt.Location = new System.Drawing.Point(176, 166);
            this.EmailTxt.Name = "EmailTxt";
            this.EmailTxt.Size = new System.Drawing.Size(295, 19);
            this.EmailTxt.TabIndex = 34;
            // 
            // NombreTxt
            // 
            this.NombreTxt.BackColor = System.Drawing.Color.LightGray;
            this.NombreTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.NombreTxt.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NombreTxt.Location = new System.Drawing.Point(176, 91);
            this.NombreTxt.Name = "NombreTxt";
            this.NombreTxt.Size = new System.Drawing.Size(295, 19);
            this.NombreTxt.TabIndex = 33;
            // 
            // IDProductoTxt
            // 
            this.IDProductoTxt.BackColor = System.Drawing.Color.LightGray;
            this.IDProductoTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.IDProductoTxt.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDProductoTxt.Location = new System.Drawing.Point(176, 56);
            this.IDProductoTxt.Name = "IDProductoTxt";
            this.IDProductoTxt.ReadOnly = true;
            this.IDProductoTxt.Size = new System.Drawing.Size(295, 19);
            this.IDProductoTxt.TabIndex = 32;
            // 
            // PassLbl
            // 
            this.PassLbl.AutoSize = true;
            this.PassLbl.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PassLbl.Location = new System.Drawing.Point(58, 237);
            this.PassLbl.Name = "PassLbl";
            this.PassLbl.Size = new System.Drawing.Size(99, 20);
            this.PassLbl.TabIndex = 31;
            this.PassLbl.Text = "Contraseña:";
            // 
            // UsuarioLbl
            // 
            this.UsuarioLbl.AutoSize = true;
            this.UsuarioLbl.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UsuarioLbl.Location = new System.Drawing.Point(90, 202);
            this.UsuarioLbl.Name = "UsuarioLbl";
            this.UsuarioLbl.Size = new System.Drawing.Size(67, 20);
            this.UsuarioLbl.TabIndex = 30;
            this.UsuarioLbl.Text = "Usuario:";
            // 
            // EmailLbl
            // 
            this.EmailLbl.AutoSize = true;
            this.EmailLbl.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EmailLbl.Location = new System.Drawing.Point(107, 165);
            this.EmailLbl.Name = "EmailLbl";
            this.EmailLbl.Size = new System.Drawing.Size(50, 20);
            this.EmailLbl.TabIndex = 29;
            this.EmailLbl.Text = "Email:";
            // 
            // ApellidoLbl
            // 
            this.ApellidoLbl.AutoSize = true;
            this.ApellidoLbl.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ApellidoLbl.Location = new System.Drawing.Point(84, 129);
            this.ApellidoLbl.Name = "ApellidoLbl";
            this.ApellidoLbl.Size = new System.Drawing.Size(73, 20);
            this.ApellidoLbl.TabIndex = 28;
            this.ApellidoLbl.Text = "Apellido:";
            // 
            // NombreLbl
            // 
            this.NombreLbl.AutoSize = true;
            this.NombreLbl.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NombreLbl.Location = new System.Drawing.Point(85, 90);
            this.NombreLbl.Name = "NombreLbl";
            this.NombreLbl.Size = new System.Drawing.Size(72, 20);
            this.NombreLbl.TabIndex = 27;
            this.NombreLbl.Text = "Nombre:";
            // 
            // IDLbl
            // 
            this.IDLbl.AutoSize = true;
            this.IDLbl.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDLbl.Location = new System.Drawing.Point(128, 54);
            this.IDLbl.Name = "IDLbl";
            this.IDLbl.Size = new System.Drawing.Size(29, 20);
            this.IDLbl.TabIndex = 26;
            this.IDLbl.Text = "ID:";
            // 
            // CancelarBtn
            // 
            this.CancelarBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CancelarBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.CancelarBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.CancelarBtn.FlatAppearance.BorderSize = 0;
            this.CancelarBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.CancelarBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.CancelarBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CancelarBtn.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CancelarBtn.ForeColor = System.Drawing.Color.White;
            this.CancelarBtn.Image = ((System.Drawing.Image)(resources.GetObject("CancelarBtn.Image")));
            this.CancelarBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.CancelarBtn.Location = new System.Drawing.Point(351, 282);
            this.CancelarBtn.Name = "CancelarBtn";
            this.CancelarBtn.Size = new System.Drawing.Size(120, 25);
            this.CancelarBtn.TabIndex = 25;
            this.CancelarBtn.Text = "Cancelar";
            this.CancelarBtn.UseVisualStyleBackColor = false;
            // 
            // GuardarBtn
            // 
            this.GuardarBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.GuardarBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.GuardarBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.GuardarBtn.FlatAppearance.BorderSize = 0;
            this.GuardarBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.GuardarBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.GuardarBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.GuardarBtn.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GuardarBtn.ForeColor = System.Drawing.Color.White;
            this.GuardarBtn.Image = ((System.Drawing.Image)(resources.GetObject("GuardarBtn.Image")));
            this.GuardarBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.GuardarBtn.Location = new System.Drawing.Point(222, 282);
            this.GuardarBtn.Name = "GuardarBtn";
            this.GuardarBtn.Size = new System.Drawing.Size(120, 25);
            this.GuardarBtn.TabIndex = 24;
            this.GuardarBtn.Text = "Guardar";
            this.GuardarBtn.UseVisualStyleBackColor = false;
            // 
            // NuevoUsuarioLbl
            // 
            this.NuevoUsuarioLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.NuevoUsuarioLbl.AutoSize = true;
            this.NuevoUsuarioLbl.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NuevoUsuarioLbl.Location = new System.Drawing.Point(12, 3);
            this.NuevoUsuarioLbl.Name = "NuevoUsuarioLbl";
            this.NuevoUsuarioLbl.Size = new System.Drawing.Size(163, 23);
            this.NuevoUsuarioLbl.TabIndex = 23;
            this.NuevoUsuarioLbl.Text = "NUEVO USUARIO";
            // 
            // NuevoUsuario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(483, 327);
            this.Controls.Add(this.CerrarBtn);
            this.Controls.Add(this.PassTxt);
            this.Controls.Add(this.UsuarioTxt);
            this.Controls.Add(this.ApellidoTxt);
            this.Controls.Add(this.EmailTxt);
            this.Controls.Add(this.NombreTxt);
            this.Controls.Add(this.IDProductoTxt);
            this.Controls.Add(this.PassLbl);
            this.Controls.Add(this.UsuarioLbl);
            this.Controls.Add(this.EmailLbl);
            this.Controls.Add(this.ApellidoLbl);
            this.Controls.Add(this.NombreLbl);
            this.Controls.Add(this.IDLbl);
            this.Controls.Add(this.CancelarBtn);
            this.Controls.Add(this.GuardarBtn);
            this.Controls.Add(this.NuevoUsuarioLbl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "NuevoUsuario";
            this.Opacity = 0.9D;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Nuevo Usuario";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label CerrarBtn;
        public System.Windows.Forms.TextBox PassTxt;
        public System.Windows.Forms.TextBox UsuarioTxt;
        public System.Windows.Forms.TextBox ApellidoTxt;
        public System.Windows.Forms.TextBox EmailTxt;
        public System.Windows.Forms.TextBox NombreTxt;
        public System.Windows.Forms.TextBox IDProductoTxt;
        private System.Windows.Forms.Label PassLbl;
        private System.Windows.Forms.Label UsuarioLbl;
        private System.Windows.Forms.Label EmailLbl;
        private System.Windows.Forms.Label ApellidoLbl;
        private System.Windows.Forms.Label NombreLbl;
        private System.Windows.Forms.Label IDLbl;
        private System.Windows.Forms.Button CancelarBtn;
        private System.Windows.Forms.Button GuardarBtn;
        private System.Windows.Forms.Label NuevoUsuarioLbl;
    }
}