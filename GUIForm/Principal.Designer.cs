﻿namespace GUIForm
{
    partial class Formulario
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Formulario));
            this.MenuVertical = new System.Windows.Forms.Panel();
            this.PresupuestosBtn = new System.Windows.Forms.Button();
            this.ClientesBtn = new System.Windows.Forms.Button();
            this.ProductosBtn = new System.Windows.Forms.Button();
            this.UsuariosBtn = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.BarraTitulo = new System.Windows.Forms.Panel();
            this.RestaurarBtn = new System.Windows.Forms.PictureBox();
            this.MinimizarBtn = new System.Windows.Forms.PictureBox();
            this.MaximizarBtn = new System.Windows.Forms.PictureBox();
            this.CerrarBtn = new System.Windows.Forms.PictureBox();
            this.BtnSlide = new System.Windows.Forms.PictureBox();
            this.PanelContenedor = new System.Windows.Forms.Panel();
            this.MenuVertical.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.BarraTitulo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RestaurarBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinimizarBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaximizarBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CerrarBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BtnSlide)).BeginInit();
            this.SuspendLayout();
            // 
            // MenuVertical
            // 
            this.MenuVertical.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.MenuVertical.Controls.Add(this.PresupuestosBtn);
            this.MenuVertical.Controls.Add(this.ClientesBtn);
            this.MenuVertical.Controls.Add(this.ProductosBtn);
            this.MenuVertical.Controls.Add(this.UsuariosBtn);
            this.MenuVertical.Controls.Add(this.pictureBox1);
            this.MenuVertical.Dock = System.Windows.Forms.DockStyle.Left;
            this.MenuVertical.Location = new System.Drawing.Point(0, 0);
            this.MenuVertical.Name = "MenuVertical";
            this.MenuVertical.Size = new System.Drawing.Size(250, 650);
            this.MenuVertical.TabIndex = 0;
            this.MenuVertical.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // PresupuestosBtn
            // 
            this.PresupuestosBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.PresupuestosBtn.FlatAppearance.BorderSize = 0;
            this.PresupuestosBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.PresupuestosBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.PresupuestosBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PresupuestosBtn.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PresupuestosBtn.ForeColor = System.Drawing.Color.White;
            this.PresupuestosBtn.Image = ((System.Drawing.Image)(resources.GetObject("PresupuestosBtn.Image")));
            this.PresupuestosBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.PresupuestosBtn.Location = new System.Drawing.Point(1, 358);
            this.PresupuestosBtn.Name = "PresupuestosBtn";
            this.PresupuestosBtn.Size = new System.Drawing.Size(250, 45);
            this.PresupuestosBtn.TabIndex = 3;
            this.PresupuestosBtn.Text = "Presupuestos";
            this.PresupuestosBtn.UseVisualStyleBackColor = true;
            // 
            // ClientesBtn
            // 
            this.ClientesBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.ClientesBtn.FlatAppearance.BorderSize = 0;
            this.ClientesBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.ClientesBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.ClientesBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ClientesBtn.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClientesBtn.ForeColor = System.Drawing.Color.White;
            this.ClientesBtn.Image = ((System.Drawing.Image)(resources.GetObject("ClientesBtn.Image")));
            this.ClientesBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ClientesBtn.Location = new System.Drawing.Point(0, 287);
            this.ClientesBtn.Name = "ClientesBtn";
            this.ClientesBtn.Size = new System.Drawing.Size(250, 45);
            this.ClientesBtn.TabIndex = 2;
            this.ClientesBtn.Text = "Clientes";
            this.ClientesBtn.UseVisualStyleBackColor = true;
            this.ClientesBtn.Click += new System.EventHandler(this.ClientesBtn_Click);
            // 
            // ProductosBtn
            // 
            this.ProductosBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.ProductosBtn.FlatAppearance.BorderSize = 0;
            this.ProductosBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.ProductosBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.ProductosBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ProductosBtn.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ProductosBtn.ForeColor = System.Drawing.Color.White;
            this.ProductosBtn.Image = ((System.Drawing.Image)(resources.GetObject("ProductosBtn.Image")));
            this.ProductosBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ProductosBtn.Location = new System.Drawing.Point(0, 220);
            this.ProductosBtn.Name = "ProductosBtn";
            this.ProductosBtn.Size = new System.Drawing.Size(250, 45);
            this.ProductosBtn.TabIndex = 1;
            this.ProductosBtn.Text = "Productos";
            this.ProductosBtn.UseVisualStyleBackColor = true;
            this.ProductosBtn.Click += new System.EventHandler(this.ProductosBtn_Click);
            // 
            // UsuariosBtn
            // 
            this.UsuariosBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.UsuariosBtn.FlatAppearance.BorderSize = 0;
            this.UsuariosBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.UsuariosBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.UsuariosBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.UsuariosBtn.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UsuariosBtn.ForeColor = System.Drawing.Color.White;
            this.UsuariosBtn.Image = ((System.Drawing.Image)(resources.GetObject("UsuariosBtn.Image")));
            this.UsuariosBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.UsuariosBtn.Location = new System.Drawing.Point(0, 152);
            this.UsuariosBtn.Name = "UsuariosBtn";
            this.UsuariosBtn.Size = new System.Drawing.Size(250, 45);
            this.UsuariosBtn.TabIndex = 0;
            this.UsuariosBtn.Text = "Admin Usuarios";
            this.UsuariosBtn.UseVisualStyleBackColor = true;
            this.UsuariosBtn.Click += new System.EventHandler(this.UsuariosBtn_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(-14, 6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(188, 108);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // BarraTitulo
            // 
            this.BarraTitulo.BackColor = System.Drawing.Color.WhiteSmoke;
            this.BarraTitulo.Controls.Add(this.RestaurarBtn);
            this.BarraTitulo.Controls.Add(this.MinimizarBtn);
            this.BarraTitulo.Controls.Add(this.MaximizarBtn);
            this.BarraTitulo.Controls.Add(this.CerrarBtn);
            this.BarraTitulo.Controls.Add(this.BtnSlide);
            this.BarraTitulo.Dock = System.Windows.Forms.DockStyle.Top;
            this.BarraTitulo.Location = new System.Drawing.Point(250, 0);
            this.BarraTitulo.Name = "BarraTitulo";
            this.BarraTitulo.Size = new System.Drawing.Size(1050, 50);
            this.BarraTitulo.TabIndex = 1;
            this.BarraTitulo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.BarraTitulo_MouseDown);
            // 
            // RestaurarBtn
            // 
            this.RestaurarBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RestaurarBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.RestaurarBtn.Image = ((System.Drawing.Image)(resources.GetObject("RestaurarBtn.Image")));
            this.RestaurarBtn.Location = new System.Drawing.Point(994, 8);
            this.RestaurarBtn.Name = "RestaurarBtn";
            this.RestaurarBtn.Size = new System.Drawing.Size(20, 20);
            this.RestaurarBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.RestaurarBtn.TabIndex = 3;
            this.RestaurarBtn.TabStop = false;
            this.RestaurarBtn.Visible = false;
            this.RestaurarBtn.Click += new System.EventHandler(this.RestaurarBtn_Click);
            // 
            // MinimizarBtn
            // 
            this.MinimizarBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.MinimizarBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.MinimizarBtn.Image = ((System.Drawing.Image)(resources.GetObject("MinimizarBtn.Image")));
            this.MinimizarBtn.Location = new System.Drawing.Point(969, 8);
            this.MinimizarBtn.Name = "MinimizarBtn";
            this.MinimizarBtn.Size = new System.Drawing.Size(20, 20);
            this.MinimizarBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.MinimizarBtn.TabIndex = 2;
            this.MinimizarBtn.TabStop = false;
            this.MinimizarBtn.Click += new System.EventHandler(this.MinimizarBtn_Click);
            // 
            // MaximizarBtn
            // 
            this.MaximizarBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.MaximizarBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.MaximizarBtn.Image = ((System.Drawing.Image)(resources.GetObject("MaximizarBtn.Image")));
            this.MaximizarBtn.Location = new System.Drawing.Point(995, 8);
            this.MaximizarBtn.Name = "MaximizarBtn";
            this.MaximizarBtn.Size = new System.Drawing.Size(20, 20);
            this.MaximizarBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.MaximizarBtn.TabIndex = 1;
            this.MaximizarBtn.TabStop = false;
            this.MaximizarBtn.Click += new System.EventHandler(this.MaximizarBtn_Click);
            // 
            // CerrarBtn
            // 
            this.CerrarBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CerrarBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CerrarBtn.Image = ((System.Drawing.Image)(resources.GetObject("CerrarBtn.Image")));
            this.CerrarBtn.Location = new System.Drawing.Point(1020, 8);
            this.CerrarBtn.Name = "CerrarBtn";
            this.CerrarBtn.Size = new System.Drawing.Size(20, 20);
            this.CerrarBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.CerrarBtn.TabIndex = 0;
            this.CerrarBtn.TabStop = false;
            this.CerrarBtn.Click += new System.EventHandler(this.CerarBtn_Click);
            // 
            // BtnSlide
            // 
            this.BtnSlide.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnSlide.Image = ((System.Drawing.Image)(resources.GetObject("BtnSlide.Image")));
            this.BtnSlide.Location = new System.Drawing.Point(6, 8);
            this.BtnSlide.Name = "BtnSlide";
            this.BtnSlide.Size = new System.Drawing.Size(35, 35);
            this.BtnSlide.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.BtnSlide.TabIndex = 0;
            this.BtnSlide.TabStop = false;
            this.BtnSlide.Click += new System.EventHandler(this.BtnSlide_Click);
            // 
            // PanelContenedor
            // 
            this.PanelContenedor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelContenedor.Location = new System.Drawing.Point(250, 50);
            this.PanelContenedor.Name = "PanelContenedor";
            this.PanelContenedor.Size = new System.Drawing.Size(1050, 600);
            this.PanelContenedor.TabIndex = 2;
            // 
            // Formulario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1300, 650);
            this.Controls.Add(this.PanelContenedor);
            this.Controls.Add(this.BarraTitulo);
            this.Controls.Add(this.MenuVertical);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Formulario";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.MenuVertical.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.BarraTitulo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RestaurarBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinimizarBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaximizarBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CerrarBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BtnSlide)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel MenuVertical;
        private System.Windows.Forms.Panel BarraTitulo;
        private System.Windows.Forms.Panel PanelContenedor;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox BtnSlide;
        private System.Windows.Forms.PictureBox CerrarBtn;
        private System.Windows.Forms.PictureBox MinimizarBtn;
        private System.Windows.Forms.PictureBox MaximizarBtn;
        private System.Windows.Forms.PictureBox RestaurarBtn;
        private System.Windows.Forms.Button UsuariosBtn;
        private System.Windows.Forms.Button ProductosBtn;
        private System.Windows.Forms.Button ClientesBtn;
        private System.Windows.Forms.Button PresupuestosBtn;
    }
}

