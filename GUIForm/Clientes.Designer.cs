﻿namespace GUIForm
{
    partial class Clientes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Clientes));
            this.EditarBtn = new System.Windows.Forms.Button();
            this.Borrar = new System.Windows.Forms.Button();
            this.NuevoBtn = new System.Windows.Forms.Button();
            this.ClientesDgv = new System.Windows.Forms.DataGridView();
            this.CerrarBtn = new System.Windows.Forms.Label();
            this.ClientesLbl = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.ClientesDgv)).BeginInit();
            this.SuspendLayout();
            // 
            // EditarBtn
            // 
            this.EditarBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.EditarBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.EditarBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.EditarBtn.FlatAppearance.BorderSize = 0;
            this.EditarBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.EditarBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.EditarBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.EditarBtn.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EditarBtn.ForeColor = System.Drawing.Color.White;
            this.EditarBtn.Image = ((System.Drawing.Image)(resources.GetObject("EditarBtn.Image")));
            this.EditarBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.EditarBtn.Location = new System.Drawing.Point(783, 71);
            this.EditarBtn.Name = "EditarBtn";
            this.EditarBtn.Size = new System.Drawing.Size(120, 25);
            this.EditarBtn.TabIndex = 13;
            this.EditarBtn.Text = "Editar";
            this.EditarBtn.UseVisualStyleBackColor = false;
            this.EditarBtn.Click += new System.EventHandler(this.EditarBtn_Click);
            // 
            // Borrar
            // 
            this.Borrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Borrar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.Borrar.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.Borrar.FlatAppearance.BorderSize = 0;
            this.Borrar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.Borrar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.Borrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Borrar.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Borrar.ForeColor = System.Drawing.Color.White;
            this.Borrar.Image = ((System.Drawing.Image)(resources.GetObject("Borrar.Image")));
            this.Borrar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Borrar.Location = new System.Drawing.Point(920, 71);
            this.Borrar.Name = "Borrar";
            this.Borrar.Size = new System.Drawing.Size(120, 25);
            this.Borrar.TabIndex = 12;
            this.Borrar.Text = "Borrar";
            this.Borrar.UseVisualStyleBackColor = false;
            // 
            // NuevoBtn
            // 
            this.NuevoBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.NuevoBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.NuevoBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.NuevoBtn.FlatAppearance.BorderSize = 0;
            this.NuevoBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.NuevoBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.NuevoBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.NuevoBtn.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NuevoBtn.ForeColor = System.Drawing.Color.White;
            this.NuevoBtn.Image = ((System.Drawing.Image)(resources.GetObject("NuevoBtn.Image")));
            this.NuevoBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.NuevoBtn.Location = new System.Drawing.Point(647, 71);
            this.NuevoBtn.Name = "NuevoBtn";
            this.NuevoBtn.Size = new System.Drawing.Size(120, 25);
            this.NuevoBtn.TabIndex = 11;
            this.NuevoBtn.Text = "Nuevo";
            this.NuevoBtn.UseVisualStyleBackColor = false;
            this.NuevoBtn.Click += new System.EventHandler(this.NuevoBtn_Click);
            // 
            // ClientesDgv
            // 
            this.ClientesDgv.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ClientesDgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ClientesDgv.Location = new System.Drawing.Point(10, 121);
            this.ClientesDgv.Name = "ClientesDgv";
            this.ClientesDgv.Size = new System.Drawing.Size(1030, 470);
            this.ClientesDgv.TabIndex = 10;
            // 
            // CerrarBtn
            // 
            this.CerrarBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CerrarBtn.AutoSize = true;
            this.CerrarBtn.BackColor = System.Drawing.Color.WhiteSmoke;
            this.CerrarBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CerrarBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CerrarBtn.Location = new System.Drawing.Point(1022, 13);
            this.CerrarBtn.Name = "CerrarBtn";
            this.CerrarBtn.Size = new System.Drawing.Size(18, 18);
            this.CerrarBtn.TabIndex = 9;
            this.CerrarBtn.Text = "X";
            this.CerrarBtn.Click += new System.EventHandler(this.CerrarBtn_Click);
            // 
            // ClientesLbl
            // 
            this.ClientesLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.ClientesLbl.AutoSize = true;
            this.ClientesLbl.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClientesLbl.Location = new System.Drawing.Point(479, 9);
            this.ClientesLbl.Name = "ClientesLbl";
            this.ClientesLbl.Size = new System.Drawing.Size(90, 23);
            this.ClientesLbl.TabIndex = 8;
            this.ClientesLbl.Text = "CLIENTES";
            // 
            // Clientes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(1050, 600);
            this.Controls.Add(this.EditarBtn);
            this.Controls.Add(this.Borrar);
            this.Controls.Add(this.NuevoBtn);
            this.Controls.Add(this.ClientesDgv);
            this.Controls.Add(this.CerrarBtn);
            this.Controls.Add(this.ClientesLbl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Clientes";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Clientes";
            ((System.ComponentModel.ISupportInitialize)(this.ClientesDgv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button EditarBtn;
        private System.Windows.Forms.Button Borrar;
        private System.Windows.Forms.Button NuevoBtn;
        private System.Windows.Forms.DataGridView ClientesDgv;
        private System.Windows.Forms.Label CerrarBtn;
        private System.Windows.Forms.Label ClientesLbl;
    }
}