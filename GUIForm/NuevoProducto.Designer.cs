﻿namespace GUIForm
{
    partial class NuevoProducto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NuevoProducto));
            this.CerrarBtn = new System.Windows.Forms.Label();
            this.CancelarBtn = new System.Windows.Forms.Button();
            this.GuardarBtn = new System.Windows.Forms.Button();
            this.NuevoProductoLbl = new System.Windows.Forms.Label();
            this.DescrpciónTxt = new System.Windows.Forms.TextBox();
            this.PVentaTxt = new System.Windows.Forms.TextBox();
            this.NombreTxt = new System.Windows.Forms.TextBox();
            this.SKUTxt = new System.Windows.Forms.TextBox();
            this.IDProductoTxt = new System.Windows.Forms.TextBox();
            this.DescripciónLbl = new System.Windows.Forms.Label();
            this.PVentaLbl = new System.Windows.Forms.Label();
            this.SKULbl = new System.Windows.Forms.Label();
            this.NombreLbl = new System.Windows.Forms.Label();
            this.IDProductoLbl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // CerrarBtn
            // 
            this.CerrarBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CerrarBtn.AutoSize = true;
            this.CerrarBtn.BackColor = System.Drawing.Color.WhiteSmoke;
            this.CerrarBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CerrarBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CerrarBtn.Location = new System.Drawing.Point(465, 6);
            this.CerrarBtn.Name = "CerrarBtn";
            this.CerrarBtn.Size = new System.Drawing.Size(18, 18);
            this.CerrarBtn.TabIndex = 42;
            this.CerrarBtn.Text = "X";
            this.CerrarBtn.Click += new System.EventHandler(this.CerrarBtn_Click);
            // 
            // CancelarBtn
            // 
            this.CancelarBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CancelarBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.CancelarBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.CancelarBtn.FlatAppearance.BorderSize = 0;
            this.CancelarBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.CancelarBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.CancelarBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CancelarBtn.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CancelarBtn.ForeColor = System.Drawing.Color.White;
            this.CancelarBtn.Image = ((System.Drawing.Image)(resources.GetObject("CancelarBtn.Image")));
            this.CancelarBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.CancelarBtn.Location = new System.Drawing.Point(363, 321);
            this.CancelarBtn.Name = "CancelarBtn";
            this.CancelarBtn.Size = new System.Drawing.Size(120, 25);
            this.CancelarBtn.TabIndex = 41;
            this.CancelarBtn.Text = "Cancelar";
            this.CancelarBtn.UseVisualStyleBackColor = false;
            this.CancelarBtn.Click += new System.EventHandler(this.CancelarBtn_Click);
            // 
            // GuardarBtn
            // 
            this.GuardarBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.GuardarBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.GuardarBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.GuardarBtn.FlatAppearance.BorderSize = 0;
            this.GuardarBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.GuardarBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.GuardarBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.GuardarBtn.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GuardarBtn.ForeColor = System.Drawing.Color.White;
            this.GuardarBtn.Image = ((System.Drawing.Image)(resources.GetObject("GuardarBtn.Image")));
            this.GuardarBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.GuardarBtn.Location = new System.Drawing.Point(220, 321);
            this.GuardarBtn.Name = "GuardarBtn";
            this.GuardarBtn.Size = new System.Drawing.Size(120, 25);
            this.GuardarBtn.TabIndex = 40;
            this.GuardarBtn.Text = "Guardar";
            this.GuardarBtn.UseVisualStyleBackColor = false;
            this.GuardarBtn.Click += new System.EventHandler(this.GuardarBtn_Click);
            // 
            // NuevoProductoLbl
            // 
            this.NuevoProductoLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.NuevoProductoLbl.AutoSize = true;
            this.NuevoProductoLbl.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NuevoProductoLbl.Location = new System.Drawing.Point(12, 6);
            this.NuevoProductoLbl.Name = "NuevoProductoLbl";
            this.NuevoProductoLbl.Size = new System.Drawing.Size(184, 23);
            this.NuevoProductoLbl.TabIndex = 39;
            this.NuevoProductoLbl.Text = "PRODUCTO NUEVO";
            // 
            // DescrpciónTxt
            // 
            this.DescrpciónTxt.BackColor = System.Drawing.Color.LightGray;
            this.DescrpciónTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DescrpciónTxt.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DescrpciónTxt.Location = new System.Drawing.Point(188, 214);
            this.DescrpciónTxt.Multiline = true;
            this.DescrpciónTxt.Name = "DescrpciónTxt";
            this.DescrpciónTxt.Size = new System.Drawing.Size(295, 92);
            this.DescrpciónTxt.TabIndex = 52;
            // 
            // PVentaTxt
            // 
            this.PVentaTxt.BackColor = System.Drawing.Color.LightGray;
            this.PVentaTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.PVentaTxt.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PVentaTxt.Location = new System.Drawing.Point(188, 178);
            this.PVentaTxt.Name = "PVentaTxt";
            this.PVentaTxt.Size = new System.Drawing.Size(295, 19);
            this.PVentaTxt.TabIndex = 51;
            // 
            // NombreTxt
            // 
            this.NombreTxt.BackColor = System.Drawing.Color.LightGray;
            this.NombreTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.NombreTxt.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NombreTxt.Location = new System.Drawing.Point(188, 105);
            this.NombreTxt.Name = "NombreTxt";
            this.NombreTxt.Size = new System.Drawing.Size(295, 19);
            this.NombreTxt.TabIndex = 50;
            // 
            // SKUTxt
            // 
            this.SKUTxt.BackColor = System.Drawing.Color.LightGray;
            this.SKUTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.SKUTxt.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SKUTxt.Location = new System.Drawing.Point(188, 141);
            this.SKUTxt.Name = "SKUTxt";
            this.SKUTxt.Size = new System.Drawing.Size(295, 19);
            this.SKUTxt.TabIndex = 49;
            // 
            // IDProductoTxt
            // 
            this.IDProductoTxt.BackColor = System.Drawing.Color.LightGray;
            this.IDProductoTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.IDProductoTxt.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDProductoTxt.Location = new System.Drawing.Point(188, 66);
            this.IDProductoTxt.Name = "IDProductoTxt";
            this.IDProductoTxt.Size = new System.Drawing.Size(295, 19);
            this.IDProductoTxt.TabIndex = 48;
            // 
            // DescripciónLbl
            // 
            this.DescripciónLbl.AutoSize = true;
            this.DescripciónLbl.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DescripciónLbl.Location = new System.Drawing.Point(70, 212);
            this.DescripciónLbl.Name = "DescripciónLbl";
            this.DescripciónLbl.Size = new System.Drawing.Size(100, 20);
            this.DescripciónLbl.TabIndex = 47;
            this.DescripciónLbl.Text = "Descripción:";
            // 
            // PVentaLbl
            // 
            this.PVentaLbl.AutoSize = true;
            this.PVentaLbl.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PVentaLbl.Location = new System.Drawing.Point(36, 177);
            this.PVentaLbl.Name = "PVentaLbl";
            this.PVentaLbl.Size = new System.Drawing.Size(133, 20);
            this.PVentaLbl.TabIndex = 46;
            this.PVentaLbl.Text = "Precio de Venta:";
            // 
            // SKULbl
            // 
            this.SKULbl.AutoSize = true;
            this.SKULbl.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SKULbl.Location = new System.Drawing.Point(129, 140);
            this.SKULbl.Name = "SKULbl";
            this.SKULbl.Size = new System.Drawing.Size(40, 20);
            this.SKULbl.TabIndex = 45;
            this.SKULbl.Text = "SKU:";
            // 
            // NombreLbl
            // 
            this.NombreLbl.AutoSize = true;
            this.NombreLbl.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NombreLbl.Location = new System.Drawing.Point(97, 104);
            this.NombreLbl.Name = "NombreLbl";
            this.NombreLbl.Size = new System.Drawing.Size(72, 20);
            this.NombreLbl.TabIndex = 44;
            this.NombreLbl.Text = "Nombre:";
            // 
            // IDProductoLbl
            // 
            this.IDProductoLbl.AutoSize = true;
            this.IDProductoLbl.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDProductoLbl.Location = new System.Drawing.Point(69, 66);
            this.IDProductoLbl.Name = "IDProductoLbl";
            this.IDProductoLbl.Size = new System.Drawing.Size(101, 20);
            this.IDProductoLbl.TabIndex = 43;
            this.IDProductoLbl.Text = "ID Producto:";
            // 
            // NuevoProducto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(500, 366);
            this.Controls.Add(this.DescrpciónTxt);
            this.Controls.Add(this.PVentaTxt);
            this.Controls.Add(this.NombreTxt);
            this.Controls.Add(this.SKUTxt);
            this.Controls.Add(this.IDProductoTxt);
            this.Controls.Add(this.DescripciónLbl);
            this.Controls.Add(this.PVentaLbl);
            this.Controls.Add(this.SKULbl);
            this.Controls.Add(this.NombreLbl);
            this.Controls.Add(this.IDProductoLbl);
            this.Controls.Add(this.CerrarBtn);
            this.Controls.Add(this.CancelarBtn);
            this.Controls.Add(this.GuardarBtn);
            this.Controls.Add(this.NuevoProductoLbl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "NuevoProducto";
            this.Opacity = 0.9D;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "NuevoProducto";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label CerrarBtn;
        private System.Windows.Forms.Button CancelarBtn;
        private System.Windows.Forms.Button GuardarBtn;
        private System.Windows.Forms.Label NuevoProductoLbl;
        public System.Windows.Forms.TextBox DescrpciónTxt;
        public System.Windows.Forms.TextBox PVentaTxt;
        public System.Windows.Forms.TextBox NombreTxt;
        public System.Windows.Forms.TextBox SKUTxt;
        public System.Windows.Forms.TextBox IDProductoTxt;
        private System.Windows.Forms.Label DescripciónLbl;
        private System.Windows.Forms.Label PVentaLbl;
        private System.Windows.Forms.Label SKULbl;
        private System.Windows.Forms.Label NombreLbl;
        private System.Windows.Forms.Label IDProductoLbl;
    }
}