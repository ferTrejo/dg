﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace GUIForm
{
    public partial class Formulario : Form
    {
        public Formulario()
        {
            InitializeComponent();
        }

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void BtnSlide_Click(object sender, EventArgs e)
        {
            if (MenuVertical.Width == 250)
            {
                MenuVertical.Width = 70;
            }
            else
                MenuVertical.Width = 250;
        }

        private void CerarBtn_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void MaximizarBtn_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            RestaurarBtn.Visible = true;
            MaximizarBtn.Visible = false;
        }

        private void RestaurarBtn_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            RestaurarBtn.Visible = false;
            MaximizarBtn.Visible = true;
        }

        private void MinimizarBtn_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;

        }

        private void BarraTitulo_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle,0x112,0xf012,0);
        }

        private void UsuariosBtn_Click(object sender, EventArgs e)
        {
            AbrirForm(new AdministracionFrm());
        }

        private void AbrirForm(object FormHijo)
        {
            if (this.PanelContenedor.Controls.Count>0) 
            this.PanelContenedor.Controls.RemoveAt(0);
            Form fh = FormHijo as Form;
            fh.TopLevel = false;
            fh.Dock = DockStyle.Fill;
            this.PanelContenedor.Controls.Add(fh);
            this.PanelContenedor.Tag = fh;
            fh.Show();
        }
        private void ProductosBtn_Click(object sender, EventArgs e)
        {
            AbrirForm(new Productos());
        }

        private void ClientesBtn_Click(object sender, EventArgs e)
        {
            AbrirForm(new Clientes());
        }
    }
}